---
title: "Cas d'utilisation de cet atelier"
date: 2019-05-17T11:20:15+02:00
draft: true
---

A l'origine, cet atelier a été imaginé pour répondre à la situation suivante :  
Une équipe agile d'une grande banque luxembourgeoise doit faire face à un changement important :  
- La scission en deux équipes avec l'apport de nouvelles ressources externes,  
- Une modification radicale du périmètre technique,  
- Et de nombreuses modifications du périmètre métier.  

Un Scrum Master orienté Raison aura tendance à vouloir animer un atelier de réflexion aboutissant à l'organisation de ces deux nouvelles équipes.  
Un Scrum Master orienté Action aura tendance à vouloir animer un atelier brainstorming orienté actions aboutissant à la création de ces deux nouvelles équipes.  
Nous proposons ici de commencer par un atelier émotion qui permettra de libérer les membres de l'équipe de leurs émotions susceptibles de perturber les capacités de réflexion ou d'action.  
